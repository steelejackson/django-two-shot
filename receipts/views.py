from django.shortcuts import render, redirect

from receipts.forms import ReceiptForm, CategoryForm, AccountForm

# Create your views here.

from receipts.models import ExpenseCategory, Account, Receipt

from django.contrib.auth.decorators import login_required

@login_required
def show_receipt_list(request):
    purchaser = Receipt.purchaser
    receipt_list = Receipt.objects.filter(purchaser=request.user)

    context = {
        'receipt_list': receipt_list
    }
    return render(request, 'lists/receipt_list.html', context)

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        'form': form,
    }

    return render(request, 'lists/create.html', context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'categories': categories,
    }
    return render(request, 'categories/category_list.html', context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        'accounts': accounts,
    }
    return render(request, 'accounts/account_list.html', context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = CategoryForm()
    context = {
        'form': form,
    }
    return render(request, 'categories/create_category.html', context)

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()
    context = {
        'form': form,
    }
    return render(request, 'accounts/create_account.html', context)
